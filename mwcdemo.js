var util = require('util');
var net = require('net');
var JsonSocket = require('json-socket');
var i2c = require('i2c');
var fs = require('fs');

var address = 0x8;
var wire = new i2c(address, {device: '/dev/i2c-1'});

var serverSocket;
var senID = 0;
var MAX_SEN_ID = 3;
var senData = new Buffer(2);

wire.scan(function(err, data) {
        console.log('I2C devices: '+data);
});

/*
console.log('Listening on: '+process.argv[2]);

net.createServer(function(sock) {
    	console.log('Connected to client on: ' + sock.remoteAddress +':'+ sock.remotePort);

    	sock = new JsonSocket(sock);
    	serverSocket = sock;

	sock.on('message', function(message) {
    		console.log('Message data:'+message.data);
	});

}).listen(process.argv[2]);
*/

setInterval(function(){

        var timestamp = new Date().getTime();
        var file = timestamp.toString()+".log";

	if (senID>=MAX_SEN_ID)
		senID = 0;

	senID++;
			
	var buf = new Buffer(1);
	buf[0] = senID;
	
	wire.readBytes(buf[0], 2, function(err, res) {
		if (res.length==2) {
			var data = (buf[0]<<8) | buf[1];
        		console.log("Received data from sensor: "+senID+" at "+timestamp+ " with value["+data+"]");
			
			var msg;
			var filename;
			if (senID == 1) {
				msg = {id:"NJlI2NaRql", d:{temperature:Math.floor(data), timestamp:timestamp}};
				filename = "nqm-iot-hub/cache/transmit/temperature.log";
			} else if (senID == 2) {
                                msg = {id:"E1g6CE6A9x", d:{accelerometer:Math.floor(data), timestamp:timestamp}};
                                filename = "nqm-iot-hub/cache/transmit/acc.log";				
			} else if (senID == 3) {
                                msg = {id:"41xtaVTCcg", d:{nfc:Math.floor(data), timestamp:timestamp}};
                                filename = "nqm-iot-hub/cache/transmit/nfc.log";
			}
	
        		fs.writeFile(filename, JSON.stringify(msg), function(err) {
            			if(err)
                			return console.log(err);
            			console.log("The file " + filename+" was saved!");
        		});
		}
		else
                	console.log("Error: incorrect data length["+res.length+"]");
	});
},5000);

#include <Wire.h>

byte data[2] = {0x00, 0x00};
char recID = 0x00;

void setup() {
  Wire.begin(8);                // join i2c bus with address #8
  Wire.onReceive(receiveEvent); // register event
  Wire.onRequest(receiveRequest);
  Serial.begin(9600);           // start serial for output
}

void loop() {
  delay(100);
}

void receiveEvent(int howMany) {
  while (Wire.available()) { // loop through all but the last
    recID = Wire.read(); // receive byte as a character
    Serial.print(recID);         // print the character
  }
}

void receiveRequest() {
  if (recID == 0x01) {
    data[0] = 0x10;
    data[1] = 0xDE;
  } else if (recID == 0x02) {
    data[0] = 0x20;
    data[1] = 0xAE;    
  } else if (recID == 0x03) {
    data[0] = 0x30;
    data[1] = 0xFE;    
  } else {
    data[0] = 0xFF;
    data[1] = 0xFF;      
  }
  
  Wire.write(data,2);
}



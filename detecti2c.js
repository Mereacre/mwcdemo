var i2c = require('i2c');
var address = 0x8;
var wire = new i2c(address, {device: '/dev/i2c-1'}); // point to your i2c address, debug provides REPL interface

wire.scan(function(err, data) {
  // result contains an array of addresses
	console.log('I2C devices: '+data);
});

wire.readBytes(0x01, 2, function(err, res) {
  // result contains a buffer of bytes
	console.log('Length: '+res.length);
	console.log(res[0]+":"+res[1]);
});


